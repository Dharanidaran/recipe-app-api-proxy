#!/bin/sh

set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/defaults.conf 
nginx -g 'daemon off;'


